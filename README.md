##Entregável aula 1 de ROS##
Nesse entregável--como pedido--criei dois nós: talker.py e listener.py.
O talker.py publica dois tópicos que ficam permanentemente repetindo-se: um com uma string de
"olá mundo" e outro com um ponto aleatório no espaço.
O listener.py recebe apenas o ponto e publica uma mensagem informando o ponto.

Ambas foram testadas.

##Entregável 1 da aula Python OO##
Nesse arquivo foi criada uma classe com os métodos que foram pedidos. O método de comparação de duas
instâncias retorna uma tupla com tuplas informando o time e a pontuação. Foram feitos os seguintes testes:

#Botafogo = Time("Botafogo",9,7,2,3,2)
#Vila_Nova = Time("Vila Nova",9,8,2,3,3)
#Cruzeiro = Time("Cruzeiro",8,8,2,2,4)
#print(Botafogo)
#print(Botafogo.comparar(Vila_Nova,Cruzeiro))
#print(Botafogo + Vila_Nova)
#print(Botafogo - Vila_Nova)

##Entregável da aula de Python básico##
A questão 1 aceita 3 parâmetros: n1, nx e razao. Se n1 for par, cria uma P.A até nx com a razão dada.
Se for ímpar, análogo; mas com uma P.G.

A questão 2 usa retorna o número de algarismos 1 na representação binária de um número. Ela usa um for loop para iterar
sobre todos os algarismos da representação binária (função bin()) e soma cada um deles para a resposta.

A questão 3 eu não consegui. Achei uma solução na internet depois de pensar muito sozinho, mas preferi não copiar porque não iria pensar naquela
solução de jeito nenhum, e fiquei preocupado de parecer desleixado ao apenas copiar.

A questão 4 simplesmente faz a soma da série passada na questão, retornando o valor e explicitando os 10 últimos algarismos.

##Entregável da aula de arduíno##
Está no outro arquivo de texto desse repositório.

##Entregável gazebo##
Não consegui fazer até sexta. Meu computador está bastante ruim e sofri muito pra mexer no gazebo, então preferi me concentrar nos outros entregáveis.


