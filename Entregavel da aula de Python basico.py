def questao1(n1,nx,razao):
    """Se n1 for par, cria uma p.a com razão <razao> até o valor nx e retorna uma lista
        com os termos. Se n1 for ímpar, cria uma p.g de forma análoga.
        int,int,int -> list"""
    resposta = [n1]
    if n1%2 == 0:
        while True:
            resposta.append(resposta[-1]+razao)
            if resposta[-1] > nx:
                resposta.pop()
                break
    else:
        while True:
            resposta.append(resposta[-1]+razao)
            if resposta[-1] > nx:
                resposta.pop()
                break

    return resposta

#print(questao1(2,13,4))

def questao2(inteiro):
    """Dado um inteiro, retorna o número de algarismos 1 na representação binária
        deste inteiro."""
    resposta = 0
    for algarismo in bin(inteiro)[2:]:
        resposta += int(algarismo)
    return resposta

#print(questao2(1234))        

def questao5():
    #não consegui. Achei uma solução na internet mas não conseguiria pensar nisso nem em outra solução, então não copiei.
    pass

def questao4():
    potencias = lambda n:n**n
    resposta = sum([potencias(n) for n in range(1000)])
    print(resposta,"Ẽ o valor da soma. Logo, os últimos dígitos são: 19110846701 ")

questao4()


