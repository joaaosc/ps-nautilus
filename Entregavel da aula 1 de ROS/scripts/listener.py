#!/usr/bin/env python3
import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Point
from std_msgs.msg import String
from random import randint

class ListenerNode():
    def __init__(self):
        rospy.init_node("ListenerNode",anonymous=True)
        self.sub = rospy.Subscriber("ponto_aleatorio",Point,self.callback)
        self.pub = rospy.Publisher("mensageiro",String,queue_size=10)
        rospy.spin()

    def callback(self,msg):
        x,y,z = msg.x,msg.y,msg.z
        mensagem = f'Posicao: ({msg.x},{msg.y},{msg.z})'
        self.pub.publish(mensagem)

if __name__ == '__main__':
    try:
        L = ListenerNode()
    except rospy.ROSInterruptException:
         pass


