#!/usr/bin/env python3
# license removed for brevity
import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String
from random import randint

class TalkerNode():
    def __init__(self):
        rospy.init_node('talker', anonymous=True)

        self.pub = rospy.Publisher('ola_mundo_topic', String, queue_size=10)
        self.pub2 = rospy.Publisher('ponto_aleatorio',Point,queue_size=1)
    
    def start(self):
        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():
            f = 'ola mundo!'
            self.pub.publish(f)

            p = Point()
            p.x,p.y,p.z = randint(-3,3),randint(-3,3),randint(-3,3)
            self.pub2.publish(p)

            rate.sleep()

if __name__ == '__main__':
    try:
        t = TalkerNode()
        t.start()
    except rospy.ROSInterruptException:
         pass
